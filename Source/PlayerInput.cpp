#include <ChunkOfLand/Debugging.hpp>
#include <ChunkOfLand/PlayerInput.hpp>

void PlayerInput::CurrentToPrevious()
{
	memcpy(m_key_state_prev, m_key_state_curr, sizeof(BYTE)*256);
	m_mouse_position_prev = m_mouse_position_curr;
}
void PlayerInput::PollKeyboard()
{
	GetKeyboardState(m_key_state_curr);
}
void PlayerInput::PollMouse()
{
	m_key_state_curr[VK_LBUTTON] = GetAsyncKeyState(VK_LBUTTON) >> 0xf;
	m_key_state_curr[VK_RBUTTON] = GetAsyncKeyState(VK_RBUTTON) >> 0xf;

	GetCursorPos(&m_mouse_position_curr);
	ScreenToClient(m_window_handle, &m_mouse_position_curr);

	m_mouse_position_delta.x = m_mouse_position_curr.x - m_mouse_position_prev.x;
	m_mouse_position_delta.y = m_mouse_position_curr.y - m_mouse_position_prev.y;

	if(m_mouse_position_locked)
	{
		POINT center_of_client = {384, 384};
		m_mouse_position_curr = center_of_client;
		ClientToScreen(m_window_handle, &center_of_client);
		SetCursorPos(center_of_client.x, center_of_client.y);
	}
}

//! Constructor
/**
	@param [in] handle Handle to the game window.
*/
PlayerInput::PlayerInput(HWND handle)
: m_window_handle(handle),
  m_mouse_position_locked(false)
{
	ZeroMemory(m_key_state_prev, sizeof(BYTE)*256);
	ZeroMemory(m_key_state_curr, sizeof(BYTE)*256);
	GetCursorPos(&m_mouse_position_curr);
	ZeroMemory(&m_mouse_position_prev, sizeof(POINT));
}
//! Destructor
PlayerInput::~PlayerInput()
{
}

//! Get the state of the mouse and keyboard.
/**
	This function obtains the state of every key on the mouse and keyboard,
	determines the position of the mouse cursor, and dispatches messages to
	the global message manager.
*/
void PlayerInput::PollInputDevices()
{
	// CurrentToPrevious must be called before polling any devices.
	CurrentToPrevious();

	PollKeyboard();
	PollMouse();
}
//! Determine whether or not a key is up.
/**
	@param key Virtual key-code of the key to check.
	@return True if the passed key is up.
*/
bool PlayerInput::IsKeyUp(const int key)
{
	return !IsKeyDown(key);
}
//! Determine whether or not a key is down.
/**
	@param key Virtual key-code of the key to check.
	@return True if the passed key is down.
*/
bool PlayerInput::IsKeyDown(const int key)
{
	return !!(m_key_state_curr[key] & 0x80);
}
//! Determine whether or not a key was pressed.
/**
	A key is pressed when it changes from Up to Down.

	@param key Virtual key-code of the key to check.
	@return True if the key was pressed.
*/
bool PlayerInput::IsKeyPressed(const int key)
{
	return !!(((~m_key_state_prev[key]) &m_key_state_curr[key]) & 0x80);
}
//! Determine whether or not a key was released.
/**
	A key is released when it goes from Down to Up.

	@param key Virtual key-code of the key to check.
	@return True if the key was released.
*/
bool PlayerInput::IsKeyReleased(const int key)
{
	return !!((m_key_state_prev[key] & (~m_key_state_curr[key])) & 0x80);
}
//! Get the position of the mouse cursor.
/**
	@return The position of the mouse cursor.
*/
POINT PlayerInput::GetMousePos()
{
	return m_mouse_position_curr;
}
POINT PlayerInput::GetMousePosDelta()
{
	return m_mouse_position_delta;
}

void PlayerInput::LockMousePos(const bool lock)
{
	m_mouse_position_locked = lock;
	ShowCursor(!lock);
}
bool PlayerInput::IsMousePosLocked()
{
	return m_mouse_position_locked;
}
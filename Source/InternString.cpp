#include <ChunkOfLand/InternString.hpp>

const HashedID InternString(const std::wstring& str)
{
	HashedID hash(0);
	MurmurHash3_x86_32(str.c_str(), str.length()*sizeof(wchar_t), 0, &hash);
	return hash;
}
const HashedID InternString(const wchar_t* const c_string, const unsigned length)
{
	HashedID hash(0);
	MurmurHash3_x86_32(c_string, length*sizeof(wchar_t), 0, &hash);
	return hash;
}
#include <ChunkOfLand/Debugging.hpp>
#include <ChunkOfLand/Window.hpp>


// Forward Declarations
LRESULT CALLBACK MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

void Window::InitWindow()
{
	WNDCLASS wc;
	ZeroMemory(&wc, sizeof(wc));
	wc.style         = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc   = MainWndProc; 
	wc.cbClsExtra    = 0;
	wc.cbWndExtra    = 0;
	wc.hInstance     = m_instance;
	wc.hIcon         = LoadIcon(0, IDI_APPLICATION);
	wc.hCursor       = LoadCursor(0, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)GetStockObject(NULL_BRUSH);
	wc.lpszMenuName  = 0;
	wc.lpszClassName = L"D3DWndClassName";

	if( !RegisterClass(&wc) )
	{
		DbgMsgToChannel(DC_RENDERER, L"RegisterClass FAILED");
		PostQuitMessage(0);
	}

	// Compute window rectangle dimensions based on requested client area dimensions.
	RECT R = { 0, 0, m_window_width, m_window_height };
	AdjustWindowRect(&R, WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX, false);
	int width  = R.right - R.left;
	int height = R.bottom - R.top;

	m_handle = CreateWindow(L"D3DWndClassName", m_title.c_str(), 
		WS_OVERLAPPED | WS_CAPTION | WS_SYSMENU | WS_MINIMIZEBOX, CW_USEDEFAULT, CW_USEDEFAULT, width, height, 0, 0, m_instance, this); 
	if( !m_handle )
	{
		DbgMsgToChannel(DC_RENDERER, L"CreateWindow FAILED");
		PostQuitMessage(0);
	}

	ShowWindow(m_handle, SW_SHOW);

	UpdateWindow(m_handle);
}

// Constructors and Destructor
Window::Window(const std::wstring& title, const unsigned window_width, const unsigned window_height)
: m_title(title),
  m_window_width(window_width),
  m_window_height(window_height),
  m_handle(0)
{
	InitWindow();
}
Window::~Window()
{
}

// Accessors and Mutators

LRESULT Window::msgProc(UINT msg, WPARAM wParam, LPARAM lParam)
{
	// Is the application in a minimized or maximized state?
	static bool minOrMaxed = false;

	RECT clientRect = {0, 0, 0, 0};
	switch( msg )
	{
	// WM_ACTIVE is sent when the window is activated or deactivated.
	// We pause the game when the main window is deactivated and 
	// unpause it when it becomes active.
	case WM_ACTIVATE:
		if( LOWORD(wParam) == WA_INACTIVE)
		{
			m_active = false;
		}
		else
		{
			m_active = true;
		}
		return 0;

	//// WM_SIZE is sent when the user resizes the window.  
	//case WM_SIZE:
	//	if( mD3DDevice )
	//	{
	//		mD3DPresentParams.BackBufferWidth  = LOWORD(lParam);
	//		mD3DPresentParams.BackBufferHeight = HIWORD(lParam);

	//		if( wParam == SIZE_MINIMIZED )
	//		{
	//			minOrMaxed = true;
	//		}
	//		else if( wParam == SIZE_MAXIMIZED )
	//		{
	//			minOrMaxed = true;
	//			OnLostDevice();
	//			HR(mD3DDevice->Reset(&mD3DPresentParams));
	//			OnResetDevice();
	//		}
	//		// Restored is any resize that is not a minimize or maximize.
	//		// For example, restoring the window to its default size
	//		// after a minimize or maximize, or from dragging the resize
	//		// bars.
	//		else if( wParam == SIZE_RESTORED )
	//		{
	//			// Are we restoring from a mimimized or maximized state, 
	//			// and are in windowed mode?  Do not execute this code if 
	//			// we are restoring to full screen mode.
	//			if( minOrMaxed && mD3DPresentParams.Windowed )
	//			{
	//				OnLostDevice();
	//				HR(mD3DDevice->Reset(&mD3DPresentParams));
	//				OnResetDevice();
	//			}
	//			else
	//			{
	//				// No, which implies the user is resizing by dragging
	//				// the resize bars.  However, we do not reset the device
	//				// here because as the user continuously drags the resize
	//				// bars, a stream of WM_SIZE messages is sent to the window,
	//				// and it would be pointless (and slow) to reset for each
	//				// WM_SIZE message received from dragging the resize bars.
	//				// So instead, we reset after the user is done resizing the
	//				// window and releases the resize bars, which sends a
	//				// WM_EXITSIZEMOVE message.
	//			}
	//			minOrMaxed = false;
	//		}
	//	}
	//	return 0;


	//// WM_EXITSIZEMOVE is sent when the user releases the resize bars.
	//// Here we reset everything based on the new window dimensions.
	//case WM_EXITSIZEMOVE:
	//	GetClientRect(m_handle, &clientRect);
	//	mD3DPresentParams.BackBufferWidth  = clientRect.right;
	//	mD3DPresentParams.BackBufferHeight = clientRect.bottom;
	//	OnLostDevice();
	//	HR(mD3DDevice->Reset(&mD3DPresentParams));
	//	OnResetDevice();

	//	return 0;

	// WM_CLOSE is sent when the user presses the 'X' button in the
	// caption bar menu.
	case WM_CLOSE:
		DestroyWindow(m_handle);
		return 0;

	// WM_DESTROY is sent when the window is being destroyed.
	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;
	}
	return DefWindowProc(m_handle, msg, wParam, lParam);
}

// Implementation of forward declaration.
LRESULT CALLBACK
MainWndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
	static Window* app = 0;

	switch( msg )
	{
		case WM_CREATE:
		{
			// Get the 'this' pointer we passed to CreateWindow via the lpParam parameter.
			CREATESTRUCT* cs = (CREATESTRUCT*)lParam;
			app = (Window*)cs->lpCreateParams;
			return 0;
		}
	}

	// Don't start processing messages until after WM_CREATE.
	if( app )
		return app->msgProc(msg, wParam, lParam);
	else
		return DefWindowProc(hwnd, msg, wParam, lParam);
}
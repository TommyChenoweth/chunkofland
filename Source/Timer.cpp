#include <boost/format.hpp>
#include <boost/mem_fn.hpp>
#include <ChunkOfLand/Debugging.hpp>
#include <ChunkOfLand/Timer.hpp>
#include <string>

void Timer::CreateFont(PD3DDevice d3ddevice)
{
	D3DXFONT_DESC fontDesc;
	fontDesc.Height				= 18;
	fontDesc.Width				= 0;
	fontDesc.Weight				= 0;
	fontDesc.MipLevels			= 1;
	fontDesc.Italic				= false;
	fontDesc.CharSet			= DEFAULT_CHARSET;
	fontDesc.OutputPrecision	= OUT_DEFAULT_PRECIS;
	fontDesc.Quality			= DEFAULT_QUALITY;
	fontDesc.PitchAndFamily		= DEFAULT_PITCH | FF_DONTCARE;

	std::wstring fontName(L"Times New Roman");
	wcsncpy_s(fontDesc.FaceName, sizeof(fontDesc.FaceName)/sizeof(WORD), fontName.c_str(), 32);

	ID3DXFont* pFont(0);
	D3DXCreateFontIndirect(d3ddevice.get(), &fontDesc, &pFont);
	m_font = PD3DXFont(pFont, boost::mem_fn(&IUnknown::Release));
}
__int64 Timer::GetCounts()
{
	__int64 cnts(0);
	QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*>(&cnts));
	return cnts;
}

//! Constructor
Timer::Timer(PD3DDevice d3ddevice, PD3DXSprite sprite)
: m_sprite(sprite),
  m_text_buffer(L""),
  m_fps(0.0f),
  m_ms_per_frame(0.0f),
  m_pause_count(0),
  m_paused(false),
  m_resumed(false),
  m_counts_this_frame(GetCounts()),
  m_counts_last_frame(m_counts_this_frame),
  m_seconds_per_count(0.0f),
  m_dt(0.0f)
{
	CreateFont(d3ddevice);
	m_text_color = D3DCOLOR_XRGB(0, 0, 0);

	__int64 cntsPerSecond(0);
	QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*>(&cntsPerSecond));
	m_seconds_per_count = 1.0f/static_cast<float>(cntsPerSecond);
}
//! Destructor
Timer::~Timer()
{
}

// Accessors and Mutators
//! Get the time elapsed since the previous frame.
float Timer::GetDT() const
{
	return m_dt;
}
//! Set the color of the rendered text
/**
	@param text_color The color that the text should be rendered.
*/
void Timer::SetTextColor(const D3DCOLOR& text_color)
{
	m_text_color = text_color;
}
//! Pause the timer.
void Timer::Pause()
{
	m_pause_count++;
	if(0 < m_pause_count)
	{
		m_paused = true;
	}
	DbgMsgToChannel(DC_TIMING, L"Pause Count: %i\n", m_pause_count);
}
//! Resume the timer.
void Timer::Resume()
{
	m_pause_count--;
	if(m_pause_count <= 0)
	{
		m_paused = false;
		m_resumed = true;
	}
	DbgMsgToChannel(DC_TIMING, L"Pause Count: %i\n", m_pause_count);
}
//! Check pause status of the timer.
/**
	@return True if the timer is paused.
*/
bool Timer::IsPaused() const
{
	return m_paused;
}
//! Update the timer.
/**
	Each time this function is called, a new DT is calculated.
*/
void Timer::UpdateTimer()
{
	static float timeElapsed(0);
	static int numFrames(0);

	if(m_resumed)
	{
		m_counts_this_frame = GetCounts();
		m_counts_last_frame = m_counts_this_frame;
		m_resumed = false;
	}
	else
	{
		m_counts_last_frame = m_counts_this_frame;
		m_counts_this_frame = GetCounts();
	}

	m_dt = (m_counts_this_frame - m_counts_last_frame) * m_seconds_per_count;
	timeElapsed += m_dt;

	numFrames++;

	// Compute FPS and MSPF every second.
	if(1.0f <= timeElapsed)
	{
		m_fps = static_cast<float>(numFrames);
		numFrames = 0;

		m_ms_per_frame = 1000.0f/m_fps;

		m_text_buffer = boost::str( boost::wformat(L"Frames Per Second = %1%\nMilliseconds Per Frame = %2%") % m_fps % m_ms_per_frame );
		m_font->PreloadText(m_text_buffer.c_str(), m_text_buffer.size());
		
		timeElapsed = 0;
		numFrames = 0;
	}
}
//! Render the timer's data.
/**
	The data will be rendered in the top left corner of the screen.
*/
void Timer::DrawTimerText() const
{
	static RECT r = {5, 5, 0, 0};
	m_font->DrawText(m_sprite.get(), m_text_buffer.c_str(), -1, &r, DT_NOCLIP, m_text_color);
}
#include <ChunkOfLand/Camera.hpp>

void Camera::BuildViewMatrix()
{
	// Right Axis
	mViewMatrix._11 = mRight.x;
	mViewMatrix._21 = mRight.y;
	mViewMatrix._31 = mRight.z;

	// Up Axis
	mViewMatrix._12 = mUp.x;
	mViewMatrix._22 = mUp.y;
	mViewMatrix._32 = mUp.z;

	// Look Axis
	mViewMatrix._13 = mLook.x;
	mViewMatrix._23 = mLook.y;
	mViewMatrix._33 = mLook.z;

	// Position
	mViewMatrix._41 = -D3DXVec3Dot(&mPosition, &mRight);
	mViewMatrix._42 = -D3DXVec3Dot(&mPosition, &mUp);
	mViewMatrix._43 = -D3DXVec3Dot(&mPosition, &mLook);

	mViewMatrix._14 = 0.0f;
	mViewMatrix._24 = 0.0f;
	mViewMatrix._34 = 0.0f;
	mViewMatrix._44 = 1.0f;
}
void Camera::BuildYawMatrix(const float fRadians)
{
	D3DXVECTOR3 world_up(0.0f, 1.0f, 0.0f);
	D3DXMatrixRotationAxis(&mYawMatrix, &world_up, fRadians);
}
void Camera::BuildPitchMatrix(const float fRadians)
{
	D3DXMatrixRotationAxis(&mPitchMatrix, &mRight, fRadians);
}
void Camera::BuildRollMatrix(const float fRadians)
{
	D3DXMatrixRotationAxis(&mRollMatrix, &mLook, fRadians);
}

//! Constructor
Camera::Camera(const D3DXVECTOR3& v3Position)
: mRight(1.0f, 0.0f, 0.0f),
  mUp(0.0f, 1.0f, 0.0f),
  mLook(0.0f, 0.0f, 1.0f),
  mPosition(v3Position)
{
	BuildViewMatrix();
}
//! Destructor
Camera::~Camera()
{
}

// Accessors and Mutators
//! Get the position of the camera.
/**
	@return The position of the camera.
 */
D3DXVECTOR3 Camera::GetPosition() const
{
	return mPosition;
}
//! Set the position of the camera.
/**
	@param [in] v3Position The new camera position.
 */
void Camera::SetPosition(const D3DXVECTOR3& v3Position)
{
	mPosition = v3Position;
	BuildViewMatrix();
}
//! Tilt the camera left and right.
/**
	@param [in] fRadians The angle in radians to rotate.

	Rotate the camera about the world up axis.
 */
void Camera::Yaw(const float fRadians)
{
	BuildYawMatrix(fRadians);
	D3DXVec3TransformCoord(&mLook, &mLook, &mYawMatrix);
	D3DXVec3TransformCoord(&mRight, &mRight, &mYawMatrix);
	D3DXVec3TransformCoord(&mUp, &mUp, &mYawMatrix);
	BuildViewMatrix();
}
//! Tilt the camera up and down.
/**
	@param [in] fRadians The angle in radian to rotate.

	Rotate the camera about the right axis.
 */
void Camera::Pitch(const float fRadians)
{
	BuildPitchMatrix(fRadians);
	D3DXVec3TransformCoord(&mLook, &mLook, &mPitchMatrix);
	D3DXVec3TransformCoord(&mUp, &mUp, &mPitchMatrix);
	BuildViewMatrix();
}
//! Do a barrell roll!
/**
	@param [in] fRadians The angle in radians to rotate.

	Rotate the camera about the look axis.
 */
void Camera::Roll(const float fRadians)
{
	BuildRollMatrix(fRadians);
	D3DXVec3TransformCoord(&mRight, &mRight, &mRollMatrix);
	D3DXVec3TransformCoord(&mUp, &mUp, &mRollMatrix);
	BuildViewMatrix();
}
//! Get the camera's view matrix.
D3DXMATRIX Camera::GetView() const
{
	return mViewMatrix;
}
D3DXVECTOR3 Camera::GetRightAxis() const
{
	return mRight;
}
D3DXVECTOR3 Camera::GetUpAxis() const
{
	return mUp;
}
D3DXVECTOR3 Camera::GetLookAxis() const
{
	return mLook;
}

void MoveCameraAlongAxis(PCamera camera, const D3DXVECTOR3& axis, const float units)
{
	camera->SetPosition( (camera->GetPosition()+units*axis) );
}
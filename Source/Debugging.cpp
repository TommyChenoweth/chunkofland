#include <cstdio>
#include <ChunkOfLand/Debugging.hpp>
#include <string>
#include <windows.h>

using namespace std;

int g_DbgChannels = 0xffffffff;

HANDLE consoleHandle = 0;

void ChangeConsoleTextColorForChannel(const int nChannel);

#ifdef _DEBUG
/*****************************************************************************
If the current build is a debug, this function will output text to the console
if the message's channel bit is true in g_DbgChannels. The outputted text will
be colored based on its channel.
NOTE: I chose to use a c style string for the message parameter because the 
argument list isn't recovered properly if the preceeding parameter is a c++ 
string/wstring.
*****************************************************************************/
void DbgMsgToChannel(const int nChannel, const wchar_t* chpMessage, ...)
{
	if(g_DbgChannels & nChannel)
	{
		ChangeConsoleTextColorForChannel(nChannel);

		// Recover any passed arguments, and use them to format the message.
		va_list args;
		va_start(args, chpMessage);
		vwprintf(chpMessage, args);
		va_end(args);
	}
}
#else
/*****************************************************************************
If the current build is a release, there is no point in sending messages to 
the console because the console won't have been created.
*****************************************************************************/
void DbgMsgToChannel(const int nChannel, const wchar_t* chpMessage, ...)
{
	if(g_DbgChannels & nChannel)
	{
		ChangeConsoleTextColorForChannel(nChannel);

		// Recover any passed arguments, and use them to format the message.
		va_list args;
		va_start(args, chpMessage);
		vwprintf(chpMessage, args);
		va_end(args);
	}
}
#endif

/*****************************************************************************
This function changes the console's text and background colors based on the
channel passed in.
*****************************************************************************/
 void ChangeConsoleTextColorForChannel(const int nChannel)
 {
	 // We must have the handle to the console's output if we want to change its color.
	 if(!consoleHandle) consoleHandle = GetStdHandle(STD_OUTPUT_HANDLE);

	// Change the console output's color based on the channel.
	switch(nChannel)
	{
	case DC_ASSETMANAGEMENT:
		SetConsoleTextAttribute(consoleHandle, FOREGROUND_RED | FOREGROUND_INTENSITY);
		break;
	case DC_INPUT:
		SetConsoleTextAttribute(consoleHandle, FOREGROUND_GREEN | FOREGROUND_INTENSITY);
		break;
	default:
		SetConsoleTextAttribute(consoleHandle, FOREGROUND_RED | FOREGROUND_BLUE | FOREGROUND_GREEN);
		break;
	}
 }

 void DbgMsgToChannel(const int channel, ID3DXBuffer* const error_message)
 {
	if(!error_message)
		return;

	std::string ansi_error(static_cast<char*>(error_message->GetBufferPointer()));
	std::wstring unicode_error(ansi_error.begin(), ansi_error.end());
	DbgMsgToChannel(channel, L"D3D ERROR:\n", unicode_error.c_str());
	error_message->Release();
	DebugBreak();
 }
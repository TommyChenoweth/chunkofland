#include <ChunkOfLand/ChunkOfLand.hpp>

// Constructors and Destructor
ChunkOfLand::ChunkOfLand()
{
	m_window.reset(new Window(L"Chunk of Land", 768, 768));

	m_direct3d.reset(new Direct3D);
	m_direct3d->InitializeD3DInWindow(m_window->GetHWND());
	m_d3ddevice = m_direct3d->GetD3DDevice();
	m_d3dobject = m_direct3d->GetD3DObject();
	m_sprite = m_direct3d->GetD3DXSprite();

	m_timer = PTimer(new Timer(m_direct3d->GetD3DDevice(), m_direct3d->GetD3DXSprite()));

	m_renderer = PRenderer(new Renderer(m_d3ddevice, m_window->GetWidth(), m_window->GetHeight()));

	m_camera.reset(new Camera(D3DXVECTOR3(0.0f, 0.0f, -15.0f)));
	m_renderer->AddCamera(m_camera);

	m_player_input = PPlayerInput(new PlayerInput(m_window->GetHWND()));
	m_player_input->LockMousePos(true);
}
ChunkOfLand::~ChunkOfLand()
{
	m_player_input.reset();
	m_camera.reset();
	m_renderer.reset();
	m_timer.reset();
	m_sprite.reset();
	m_d3dobject.reset();
	m_d3ddevice.reset();
	m_direct3d.reset();
	m_window.reset();
}

int ChunkOfLand::Run()
{
	MSG msg;
	msg.message = WM_NULL;
	while(msg.message != WM_QUIT)
	{
		if(PeekMessage(&msg, 0, 0, 0, PM_REMOVE))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
		else
		{
			if(!m_window->IsActive())
			{
				Sleep(20);
				continue;
			}
			// If the device isn't lost...
			if(!m_direct3d->IsDeviceLost())
			{
				m_timer->UpdateTimer();
				m_player_input->PollInputDevices();
				Update(m_timer->GetDT());
				Render();
			}
		}
	}
	return (int)msg.wParam;
}
void ChunkOfLand::Update(const float dt)
{
	const float camera_speed(50.0f);
	if(m_player_input->IsKeyDown('W'))
		MoveCameraAlongAxis(m_camera, m_camera->GetLookAxis(), camera_speed*dt);
	if(m_player_input->IsKeyDown('S'))
		MoveCameraAlongAxis(m_camera, m_camera->GetLookAxis(), -camera_speed*dt);
	if(m_player_input->IsKeyDown('A'))
		MoveCameraAlongAxis(m_camera, m_camera->GetRightAxis(), -camera_speed*dt);
	if(m_player_input->IsKeyDown('D'))
		MoveCameraAlongAxis(m_camera, m_camera->GetRightAxis(), camera_speed*dt);

	const POINT camera_position_delta = m_player_input->GetMousePosDelta();
	if(camera_position_delta.x != 0)
		m_camera->Yaw(camera_position_delta.x/768.0f);
	if(camera_position_delta.y != 0)
		m_camera->Pitch(camera_position_delta.y/768.0f);

	// Unlock the mouse cursor when escape is pressed.
	if(m_player_input->IsKeyPressed(VK_ESCAPE))
		m_player_input->LockMousePos(!m_player_input->IsMousePosLocked());

	static D3DXMATRIX rotate_about_y;
	D3DXMatrixRotationAxis(&rotate_about_y, &D3DXVECTOR3(0.0f, 1.0f, 0.0f), D3DX_PI*dt*0.25f);

	D3DXVECTOR3 light_direction = m_renderer->GetDirectionalLight();
	D3DXVec3TransformCoord(&light_direction, &light_direction, &rotate_about_y);
	m_renderer->SetDirectionalLight(light_direction);
}
void ChunkOfLand::Render()
{
	m_d3ddevice->Clear(0, 0, D3DCLEAR_TARGET | D3DCLEAR_ZBUFFER, 0x770000, 1.0f, 0);
	m_d3ddevice->BeginScene();

	m_sprite->Begin(D3DXSPRITE_ALPHABLEND);
	m_timer->DrawTimerText();
	m_sprite->End();

	m_renderer->RenderGeometry();

	m_d3ddevice->EndScene();
	m_d3ddevice->Present(0, 0, 0, 0);
}
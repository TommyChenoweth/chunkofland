#include <ChunkOfLand/Model.hpp>

void Model::LoadTextures()
{
	IDirect3DTexture9* texture(0);
	D3DXCreateTextureFromFile(
		m_d3ddevice.get(),
		m_materials[0].GetTexturePath().c_str(),
		&texture);
	BuildD3DPtr(&m_color_map, &texture);

	D3DXCreateTextureFromFile(
		m_d3ddevice.get(),
		m_materials[0].GetNormalMapPath().c_str(),
		&texture);
	BuildD3DPtr(&m_normal_map, &texture);
}

// Constructors and Destructor
Model::Model(PD3DDevice d3ddevice, PModelFileParser parser, const std::wstring& path_to_model)
: m_d3ddevice(d3ddevice),
  m_number_of_materials(0),
  m_parser(parser),
  m_path_to_model(path_to_model)
{
}
Model::~Model()
{
}

// Accessors and Mutators
PD3DXMesh Model::GetMesh()
{
	return m_mesh;
}
unsigned Model::GetNumberOfSubsets() const
{
	return m_number_of_materials;
}
PD3DTexture Model::GetTextureForSubset(const unsigned subset_index)
{
	return m_color_map;
}
PD3DTexture Model::GetNormalMapForSubset(const unsigned subset_index)
{
	return m_normal_map;
}

void Model::Load()
{
	m_parser->CreateMeshFromFile(
		m_path_to_model,
		&m_adjacency,
		&m_materials,
		0,
		&m_number_of_materials,
		&m_mesh);

	LoadTextures();
}
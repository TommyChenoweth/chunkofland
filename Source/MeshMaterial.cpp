#include <ChunkOfLand/MeshMaterial.hpp>

// Constructors and Destructor
MeshMaterial::MeshMaterial()
: m_texture_path(L"NO_PATH_SET"),
  m_normal_map_path(L"NO_PATH_SET")
{
}
MeshMaterial::~MeshMaterial()
{
}

// Accessors and Mutators
std::wstring MeshMaterial::GetTexturePath() const
{
	return m_texture_path;
}
void MeshMaterial::SetTexturePath(const std::wstring& texture_path)
{
	m_texture_path = texture_path;
}
std::wstring MeshMaterial::GetNormalMapPath() const
{
	return m_normal_map_path;
}
void MeshMaterial::SetNormalMapPath(const std::wstring& normal_map_path)
{
	m_normal_map_path = normal_map_path;
}
#include <ChunkOfLand/D3DTypes.hpp>
#include <ChunkOfLand/Debugging.hpp>
#include <ChunkOfLand/Renderer.hpp>
#include <ChunkOfLand/VertexDeclarations.hpp>
#include <d3d9.h>
#include <D3DX9Mesh.h>
#include <string>

#include <ChunkOfLand/ObjParser.hpp>

void Renderer::CompileEffect()
{
	int effect_compilation_flags(0);
	#if defined(DEBUG) | defined(_DEBUG)
	effect_compilation_flags |= D3DXSHADER_DEBUG | D3DXSHADER_SKIPOPTIMIZATION;
	#else
	effect_compilation_flags |= D3DXSHADER_OPTIMIZATION_LEVEL3;
	#endif

	ID3DXEffect* effect(0);
	ID3DXBuffer* errors(0);
	D3DXCreateEffectFromFile(
		m_d3ddevice.get(),
		L"../Assets/Shaders/TexturedModel.fx",
		0,
		0,
		effect_compilation_flags,
		0,
		&effect,
		&errors);

	if(errors)
	{
		std::string ansi_error(static_cast<char*>(errors->GetBufferPointer()));
		std::wstring unicode_error(ansi_error.begin(), ansi_error.end());
		DbgMsgToChannel(DC_RENDERER, L"Error creating shader: %s\n", unicode_error.c_str());
		errors->Release();
		DebugBreak();
	}

	BuildD3DPtr(&m_effect, &effect);

	// We only have one technique in this shader, so we may as well
	// set it now.
	m_technique_handle = m_effect->GetTechniqueByName("TexturedModel");
	HR(m_effect->SetTechnique(m_technique_handle));

	// Get handles to shader variables.
	m_wvp_matrix		= m_effect->GetParameterByName(0, "g_wvp");
	m_texture_handle	= m_effect->GetParameterByName(0, "g_texture");
	m_normal_map_handle	= m_effect->GetParameterByName(0, "g_normal_map");
	m_light_direction_handle	= m_effect->GetParameterByName(0, "g_light_direction");
}

Renderer::Renderer(PD3DDevice d3ddevice, const unsigned window_width, const unsigned window_height)
: m_d3ddevice(d3ddevice),
  m_camera(new Camera(D3DXVECTOR3(0.0f, 0.0f, 0.0f))),
  m_light_direction(0.0f, 0.0f, -1.0f)
{
	CreateVertexDeclarations(m_d3ddevice);
	CompileEffect();

	D3DXMatrixIdentity(&m_projection);
	D3DXMatrixPerspectiveFovLH(
		&m_projection, 
		D3DXToRadian(75), 
		static_cast<float>(window_width)/static_cast<float>(window_height), 
		1.0f, 10000.0f);

	PObjParser obj_parser(new ObjParser(m_d3ddevice));
	m_model.reset(new Model(m_d3ddevice, obj_parser, L"../Assets/Models/col_rock_v2.obj"));
	m_model->Load();

	//m_d3ddevice->SetRenderState(D3DRS_FILLMODE, D3DFILL_WIREFRAME);
	//m_d3ddevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
}
Renderer::~Renderer()
{
}

// Accessors and Mutators
void Renderer::AddCamera(PCamera& camera)
{
	m_camera = camera;
}
void Renderer::RemoveCamera(PCamera& camera)
{
}
void Renderer::SetDirectionalLight(const D3DXVECTOR3& light_direction)
{
	m_light_direction = light_direction;
}
D3DXVECTOR3 Renderer::GetDirectionalLight() const
{
	return m_light_direction;
}

void Renderer::RenderGeometry()
{
	m_effect->SetMatrix(m_wvp_matrix, &(m_camera->GetView()*m_projection));
	m_effect->SetFloatArray(m_light_direction_handle, m_light_direction, 3);

	unsigned num_passes(0);
	m_effect->Begin(&num_passes, 0);
	for(unsigned i = 0; i < num_passes; i++)
	{
		m_effect->BeginPass(i);

		for(unsigned i = 0; i < m_model->GetNumberOfSubsets(); i++)
		{
			m_effect->SetTexture(m_texture_handle, m_model->GetTextureForSubset(i).get());
			m_effect->SetTexture(m_normal_map_handle, m_model->GetNormalMapForSubset(i).get());
			m_effect->CommitChanges();
			m_model->GetMesh()->DrawSubset(i);
		}

		m_effect->EndPass();
	}
	m_effect->End();
}
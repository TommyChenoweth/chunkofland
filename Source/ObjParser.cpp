#include <boost/algorithm/string/classification.hpp>
#include <boost/algorithm/string/replace.hpp>
#include <boost/algorithm/string/split.hpp>
#include <boost/mem_fn.hpp>
#include <ChunkOfLand/Debugging.hpp>
#include <ChunkOfLand/InternString.hpp>
#include <ChunkOfLand/ObjParser.hpp>
#include <ChunkOfLand/VertexDeclarations.hpp>
#include <exception>
#include <fstream>
#include <list>
#include <set>
#include <vector>

const HashedID SUBSET_TOKEN(InternString(L"o"));
const HashedID VERTEX_TOKEN(InternString(L"v"));
const HashedID FACE_TOKEN(InternString(L"f"));
const HashedID MATERIAL_DEFINITION_TOKEN(InternString(L"newmtl"));
const HashedID MATERIAL_LIBRARY_TOKEN(InternString(L"mtllib"));
const HashedID UV_COORDINATE_TOKEN(InternString(L"vt"));
const HashedID COLOR_MAP_TOKEN(InternString(L"map_Kd"));
const HashedID NORMAL_MAP_TOKEN(InternString(L"map_Bump"));

void ObjParser::CreateMaterialsFromFile(MeshMaterialArray* out_materials, const std::wstring& path_to_file) const
{
	try
	{
		std::wifstream material_file;
		material_file.open(path_to_file.c_str(), std::ios_base::binary | std::ios_base::in);
		if(!material_file.is_open())
			return;

		TokenToCountMap token_counts = CountAllTokensInFile(&material_file);
		unsigned number_of_materials = token_counts[MATERIAL_DEFINITION_TOKEN];
		out_materials->reset(new MeshMaterial[number_of_materials]);
		MeshMaterial* mesh_material_it = (*out_materials).get();

		/*
			The iterator doesn't work. Fix it.
		*/

		material_file.seekg(std::ios_base::beg);

		std::wstring buffer(256, L'_');
		while(!material_file.eof())
		{
			material_file.getline(&buffer[0], buffer.size());
			std::vector<std::wstring> line_elements;
			boost::algorithm::split(
					line_elements, 
					buffer, 
					boost::algorithm::is_any_of(L" "));
			HashedID token_hash(InternString(line_elements[0]));

			if(token_hash == COLOR_MAP_TOKEN)
				(*out_materials)[0].SetTexturePath(line_elements[1]);
			else if(token_hash == NORMAL_MAP_TOKEN)
				(*out_materials)[0].SetNormalMapPath(line_elements[1]);
			else if(token_hash == MATERIAL_DEFINITION_TOKEN)
				mesh_material_it++;

			material_file.peek();
		}

		material_file.close();
	}
	catch(std::exception e)
	{
		std::string ansi_error_message(e.what());
		std::wstring unicode_error_message(ansi_error_message.begin(), ansi_error_message.end());
		DbgMsgToChannel(DC_ASSETMANAGEMENT, L"ERROR: %s\n", unicode_error_message.c_str());
	}

}
ObjParser::TokenToCountMap ObjParser::CountAllTokensInFile(std::wifstream* file) const
{
	TokenToCountMap token_map;
	std::wstring buffer(256, L'_');
	file->seekg(0, std::ios::beg);
	while(!file->eof())
	{
		file->get(&buffer[0], buffer.size(), L' ');
		const HashedID token(InternString(buffer.c_str(), file->gcount()));
		token_map[token]++;
		file->ignore(256, L'\n');
		file->peek();
	}
	file->seekg(0, std::ios::beg);
	return token_map;
}

// Constructors and Destructor
ObjParser::ObjParser(PD3DDevice d3ddevice)
: m_d3ddevice(d3ddevice)
{
}
ObjParser::~ObjParser()
{
}

// IModelFileParser Implementation
HRESULT ObjParser::CreateMeshFromFile(
		const std::wstring& path_to_model,
		PD3DXBuffer* out_adjacency,
		MeshMaterialArray* out_materials,
		PD3DXBuffer* out_effects,
		unsigned* out_number_of_materials,
		PD3DXMesh* out_mesh) const
{
	std::wifstream model_file;
	model_file.exceptions(std::ios::failbit | std::ios::badbit | std::ios::end);
	try
	{
		model_file.open(path_to_model.c_str(), std::ios_base::binary | std::ios_base::in);

		TokenToCountMap token_counts = CountAllTokensInFile(&model_file);

		std::vector<D3DXMATERIAL> material_list;
		MeshMaterial* material_it = out_materials->get();

		const unsigned number_of_vertices(token_counts[VERTEX_TOKEN]);
		std::vector<D3DXVECTOR3> vertex_positions(number_of_vertices);
		std::vector<D3DXVECTOR3>::iterator vertex_position_it = vertex_positions.begin();

		const unsigned number_of_faces(token_counts[FACE_TOKEN]);
		VertexIndices vertex_indices;

		const unsigned number_of_subsets(token_counts[SUBSET_TOKEN]);
		(*out_number_of_materials) = number_of_subsets;
		std::vector<unsigned> subset_faces(number_of_subsets);
		std::vector<unsigned>::iterator subset_it = subset_faces.begin();

		const unsigned number_of_uv_coordinates(token_counts[UV_COORDINATE_TOKEN]);
		std::vector<D3DXVECTOR2> uv_coordinates(number_of_uv_coordinates);
		std::vector<D3DXVECTOR2>::iterator uv_it = uv_coordinates.begin();

		std::set<HashedID> hashed_indices;

		std::vector<short> indices;

		std::wstring buffer(256, L'_');
		while(!model_file.eof())
		{
			model_file.getline(&buffer[0], buffer.size());
			std::vector<std::wstring> line_elements;
			boost::algorithm::split(
					line_elements, 
					buffer, 
					boost::algorithm::is_any_of(L" "));

			const HashedID token(InternString(line_elements[0]));

			if(token == VERTEX_TOKEN)
			{
				vertex_position_it->x = static_cast<float>(_wtof(line_elements[1].c_str()));
				vertex_position_it->y = static_cast<float>(_wtof(line_elements[2].c_str()));
				vertex_position_it->z = static_cast<float>(_wtof(line_elements[3].c_str()));
				vertex_position_it++;
			}
			else if(token == UV_COORDINATE_TOKEN)
			{
				uv_it->x = static_cast<float>(_wtof(line_elements[1].c_str()));
				uv_it->y = static_cast<float>(_wtof(line_elements[2].c_str()));
				uv_it++;
			}
			else if(token == FACE_TOKEN)
			{
				for(unsigned i = 1; i < 4; i++)
				{

					HashedID hashed_index(InternString(line_elements[i]));

					if(!hashed_indices.insert(hashed_index).second)
					{
						indices.push_back(vertex_indices[hashed_index].absolute_index);
						continue;
					}

					indices.push_back(hashed_indices.size()-1);

					std::vector<std::wstring> split_face_data;
					boost::algorithm::split(
						split_face_data, 
						line_elements[i], 
						boost::algorithm::is_any_of(L"/"));

					vertex_indices[hashed_index].absolute_index = hashed_indices.size()-1;
					vertex_indices[hashed_index].position_vertex_index = static_cast<short>(_wtoi(split_face_data[0].c_str()))-1;
					vertex_indices[hashed_index].uv_coordinate_index = static_cast<short>(_wtoi(split_face_data[1].c_str()))-1;
				}

				(*subset_it)++;
			}
			else if(token == SUBSET_TOKEN)
			{
				static bool done_once(false);
				if(done_once)
				{
					subset_it++;
				}
				else
					done_once = true;
			}
			else if(token == MATERIAL_LIBRARY_TOKEN)
			{
				std::vector<std::wstring> path_components;
				boost::algorithm::split(
					path_components,
					path_to_model,
					boost::algorithm::is_any_of(L"/"));

				std::wstring path_to_material(path_to_model);
				boost::algorithm::replace_first(
					path_to_material,
					path_components[path_components.size()-1],
					line_elements[1]);

				CreateMaterialsFromFile(out_materials, path_to_material);
			}
			else
				DbgMsgToChannel(DC_ASSETMANAGEMENT, L"Unhandled Token: %s\n", line_elements[0].c_str());

			// Check for the end of the file.
			model_file.peek();
		}

		model_file.close();

		ID3DXMesh* mesh(0);
		HR(D3DXCreateMesh(
			number_of_faces,
			indices.size(),
			D3DXMESH_MANAGED,
			StaticMeshVertex_Description,
			m_d3ddevice.get(),
			&mesh));

		std::vector<StaticMeshVertex> vertices(hashed_indices.size());
		std::set<HashedID>::iterator hashed_index_it = hashed_indices.begin();
		while(hashed_index_it != hashed_indices.end())
		{
			short index(vertex_indices[*hashed_index_it].absolute_index);
			vertices[index].position = vertex_positions[vertex_indices[*hashed_index_it].position_vertex_index];
			vertices[index].uv		 = uv_coordinates[vertex_indices[*hashed_index_it].uv_coordinate_index];
			hashed_index_it++;
		}

		void* vertex_buffer(0);
		HR(mesh->LockVertexBuffer(0, &vertex_buffer));
		memcpy(vertex_buffer, &vertices[0], sizeof(StaticMeshVertex)*vertices.size());
		vertex_buffer = 0;
		HR(mesh->UnlockVertexBuffer());

		void* index_buffer(0);
		HR(mesh->LockIndexBuffer(0, &index_buffer));
		memcpy(index_buffer, &indices[0], sizeof(short)*indices.size());
		index_buffer = 0;
		HR(mesh->UnlockIndexBuffer());

		DWORD* attribute_buffer(0);
		HR(mesh->LockAttributeBuffer(0, &attribute_buffer));
		for(unsigned i = 1, face_index = subset_faces[0]; i < subset_faces.size(); i++)
		{
			for(unsigned j = 0; j < subset_faces[i]; j++)
				attribute_buffer[j+face_index] = i;

			face_index += subset_faces[i];
		}
		attribute_buffer = 0;
		HR(mesh->UnlockAttributeBuffer());

		boost::shared_array<DWORD> dirty_adjacency(new DWORD[mesh->GetNumFaces()*3]);
		HR(mesh->GenerateAdjacency(
			0,
			dirty_adjacency.get()));

		boost::shared_array<DWORD> clean_adjacency(new DWORD[mesh->GetNumFaces()*3]);
		ID3DXMesh* clean_mesh(0);
		ID3DXBuffer* clean_mesh_errors(0);
		HR(D3DXCleanMesh(
			D3DXCLEAN_BOWTIES,
			mesh,
			dirty_adjacency.get(),
			&clean_mesh,
			clean_adjacency.get(),
			&clean_mesh_errors));
		DbgMsgToChannel(DC_ASSETMANAGEMENT, clean_mesh_errors);
		clean_mesh_errors = 0;
		mesh->Release();

		ID3DXBuffer* validate_mesh_errors(0);
		HR(D3DXValidMesh(
			clean_mesh,
			clean_adjacency.get(),
			&validate_mesh_errors));
		DbgMsgToChannel(DC_ASSETMANAGEMENT, validate_mesh_errors);
		validate_mesh_errors = 0;

		ID3DXMesh* final_mesh(0);
		HR(D3DXComputeTangentFrameEx(
			clean_mesh,
			D3DDECLUSAGE_TEXCOORD, 0,
			D3DDECLUSAGE_TANGENT, 0,
			D3DDECLUSAGE_BINORMAL, 0,
			D3DDECLUSAGE_NORMAL, 0,
			D3DXTANGENT_CALCULATE_NORMALS,
			clean_adjacency.get(),
			0.01f, 0.25f, 0.01f,
			&final_mesh,
			0));
		clean_mesh->Release();

		BuildD3DPtr(out_mesh, &final_mesh);
	}
	catch(std::exception e)
	{
		std::string ansi_error_message(e.what());
		std::wstring unicode_error_message(ansi_error_message.begin(), ansi_error_message.end());
		DbgMsgToChannel(DC_ASSETMANAGEMENT, L"ERROR: %s\n", unicode_error_message.c_str());
	}

	return D3D_OK;
}
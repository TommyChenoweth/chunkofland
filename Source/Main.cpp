#include <ChunkOfLand/ChunkOfLand.hpp>
#include <ChunkOfLand/ObjParser.hpp>
#include <crtdbg.h>
#include <windows.h>

#include <ChunkOfLand/Model.hpp>

void AllocAndAttachConsole();

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance,
				   PSTR cmdLine, int showCmd)
{
	// Enable run-time memory check and console for debug builds.
	#if defined(DEBUG) | defined(_DEBUG)
	_CrtSetDbgFlag( _CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF );
	#endif

	AllocAndAttachConsole();

	ChunkOfLand demo;
	return demo.Run();
}

void AllocAndAttachConsole()
{
	// Create a console.
	AllocConsole();

	// Attach the created console to this application.
	AttachConsole(GetCurrentProcessId());

	// Redirect output to the attached console.
	HANDLE conOut = GetStdHandle(STD_OUTPUT_HANDLE);
	FILE* pStream(0);
	freopen_s(&pStream, "CON", "w", stdout);

	// Set the size of the console's output buffer.
	COORD crdOutBufferDimensions;
	crdOutBufferDimensions.X = 80;
	crdOutBufferDimensions.Y = 1600;
	SetConsoleScreenBufferSize(conOut, crdOutBufferDimensions);

	// Change the console's title to more accurately reflect its purpose.
	SetConsoleTitle(L"Debug Console");

	// Disable the console's close button as this seems to cause the application
	// to close ungracefully.
	RemoveMenu(GetSystemMenu(GetConsoleWindow(), false), SC_CLOSE, MF_BYCOMMAND);
	DrawMenuBar(GetConsoleWindow());
}
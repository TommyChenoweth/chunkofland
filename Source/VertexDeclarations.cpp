#include <ChunkOfLand/VertexDeclarations.hpp>

PD3DVertexDeclaration g_decl_ColorVertex;
PD3DVertexDeclaration g_decl_StaticMeshVertex;

void CreateVertexDeclarations(PD3DDevice d3ddevice)
{
	IDirect3DVertexDeclaration9* vertex_declaration(0);
	d3ddevice->CreateVertexDeclaration(ColorVertex_Description, &vertex_declaration);
	BuildD3DPtr(&g_decl_ColorVertex, &vertex_declaration);

	d3ddevice->CreateVertexDeclaration(StaticMeshVertex_Description, &vertex_declaration);
	BuildD3DPtr(&g_decl_StaticMeshVertex, &vertex_declaration);
}
#include <boost/mem_fn.hpp>
#include <ChunkOfLand/Debugging.hpp>
#include <ChunkOfLand/Direct3D.hpp>

// Initializing Direct3D
HRESULT Direct3D::CreateD3DObject()
{
	IDirect3D9* d3dobject = Direct3DCreate9(D3D_SDK_VERSION);
	if(!d3dobject)
		return E_FAIL;
	m_d3dobject = PD3DObject(d3dobject, boost::mem_fn(&IUnknown::Release));
	return D3D_OK;
}
HRESULT Direct3D::VerifyHALAndFormatSupport()
{
	D3DDISPLAYMODE display_mode;
	m_d3dobject->GetAdapterDisplayMode(m_adapter, &display_mode);

	// Verify windowed support of HAL. We'll use whatever
	// format that the adapter is currently using.
	HRESULT windowed_support_result = m_d3dobject->CheckDeviceType(
		m_adapter,
		m_device_type,
		display_mode.Format,
		display_mode.Format,
		true);
	switch(windowed_support_result)
	{
	case D3DERR_INVALIDCALL:
		// A parameter is invalid, incorrect, or corrupt.
		HR(windowed_support_result);
		return E_FAIL;
	case D3DERR_NOTAVAILABLE:
		// The requested format isn't supported by the adapter.
		HR(windowed_support_result);
		return E_FAIL;
	}

	// Verify fullscreen support of HAL and format.
	HRESULT fullscreen_support_result = m_d3dobject->CheckDeviceType(
		m_adapter,
		m_device_type,
		m_adapter_format,
		m_backbuffer_format,
		false);
	switch(fullscreen_support_result)
	{
	case D3DERR_INVALIDCALL:
		// A parameter is invalid, incorrect, or corrupt.
		HR(fullscreen_support_result);
		return E_FAIL;
	case D3DERR_NOTAVAILABLE:
		// The requested format isn't supported by the adapter.
		HR(fullscreen_support_result);
		return E_FAIL;
	}

	return D3D_OK;
}
HRESULT Direct3D::VerifyHardwareVertexProcessing()
{
	D3DCAPS9 caps;
	HR(m_d3dobject->GetDeviceCaps(m_adapter, m_device_type, &caps));

	// If the device supports transform and light, we can use whichever
	// vertex processing we please.
	if(caps.DevCaps & D3DDEVCAPS_HWTRANSFORMANDLIGHT)
		m_device_behavior |= m_vertex_processing;
	else
	{
		// Hardware vertex processing isn't available. The only vertex
		// processing we can use is software.
		DbgMsgToChannel(DC_RENDERER, L"Hardware vertex processing isn't available. Using software instead.");
		m_device_behavior |= D3DCREATE_SOFTWARE_VERTEXPROCESSING;
	}

	// Make a pure device if possible.
	bool hardware_vp_and_pure_device_supported(caps.DevCaps & D3DDEVCAPS_PUREDEVICE && m_device_behavior & D3DCREATE_HARDWARE_VERTEXPROCESSING);
	if(hardware_vp_and_pure_device_supported)
	{
		m_device_behavior |= D3DCREATE_PUREDEVICE;
		return D3D_OK;
	}

	return D3D_OK;
}
void Direct3D::SetPresentParams(HWND& window_handle)
{
	m_present_parameters.BackBufferWidth			= 0;
	m_present_parameters.BackBufferHeight			= 0;
	m_present_parameters.BackBufferFormat			= D3DFMT_UNKNOWN;
	m_present_parameters.BackBufferCount			= 1;
	m_present_parameters.MultiSampleType			= D3DMULTISAMPLE_NONE;
	m_present_parameters.MultiSampleQuality			= 0;
	m_present_parameters.SwapEffect					= D3DSWAPEFFECT_DISCARD;
	m_present_parameters.hDeviceWindow				= window_handle;
	m_present_parameters.Windowed					= true;
	m_present_parameters.EnableAutoDepthStencil		= true;
	m_present_parameters.AutoDepthStencilFormat		= D3DFMT_D24S8;
	m_present_parameters.Flags						= 0;
	m_present_parameters.FullScreen_RefreshRateInHz	= D3DPRESENT_RATE_DEFAULT;
	#if defined(DEBUG) | defined(_DEBUG)
	m_present_parameters.PresentationInterval		= D3DPRESENT_INTERVAL_IMMEDIATE;
	//m_present_parameters.PresentationInterval		= D3DPRESENT_INTERVAL_DEFAULT;
	#else
	m_present_parameters.PresentationInterval		= D3DPRESENT_INTERVAL_IMMEDIATE;
	//m_present_parameters.PresentationInterval		= D3DPRESENT_INTERVAL_DEFAULT;
	#endif
}
HRESULT Direct3D::CreateDeviceInterface(HWND& window_handle)
{
	IDirect3DDevice9* d3ddevice(0);
	HRESULT device_created = m_d3dobject->CreateDevice(
		m_adapter,
		m_device_type,
		window_handle,
		m_device_behavior,
		&m_present_parameters,
		&d3ddevice);

	switch(device_created)
	{
	case D3DERR_DEVICELOST:
		// Somehow the device is already lost. I don't know whether
		// or not it can be recovered at this point.
		DbgMsgToChannel(DC_RENDERER, L"Can't create device: The device is lost.");
		return E_FAIL;
	case D3DERR_INVALIDCALL:
		// A parameter is incorrect, corrupt, or invalid.
		DbgMsgToChannel(DC_RENDERER, L"Can't create device: A parameter is incorrect, corrupt, or invalid.");
		return E_FAIL;
	case D3DERR_NOTAVAILABLE:
		// This device does not support the queried technique.
		DbgMsgToChannel(DC_RENDERER, L"Can't create device: The adapter doesn't support a feature.");
		return E_FAIL;
	case D3DERR_OUTOFVIDEOMEMORY:
		// Somehow the adapter is already out of video memory.
		DbgMsgToChannel(DC_RENDERER, L"Can't create device: Out of video memory.");
		return E_FAIL;
	}

	m_d3ddevice = PD3DDevice(d3ddevice, boost::mem_fn(&IUnknown::Release));

	return D3D_OK;
}

void Direct3D::CreateSprite()
{
	ID3DXSprite* sprite(0);
	HRESULT create_sprite_result = D3DXCreateSprite(m_d3ddevice.get(), &sprite);
	switch(create_sprite_result)
	{
	case D3DERR_INVALIDCALL:
		DbgMsgToChannel(DC_RENDERER, L"Can't create sprite: Invalid parameters.");
		PostQuitMessage(0);
		return;
	case E_OUTOFMEMORY:
		DbgMsgToChannel(DC_RENDERER, L"Can't create sprite: Out of memory.");
		PostQuitMessage(0);
		return;
	}
	m_sprite = PD3DXSprite(sprite, boost::mem_fn(&IUnknown::Release));
}

// Constructors and Destructor
Direct3D::Direct3D()
: m_adapter(D3DADAPTER_DEFAULT),
  m_adapter_format(D3DFMT_X8R8G8B8),
  m_vertex_processing(D3DCREATE_HARDWARE_VERTEXPROCESSING),
  m_device_behavior(0),
  m_device_type(D3DDEVTYPE_HAL),
  m_backbuffer_format(D3DFMT_X8R8G8B8)
{
	ZeroMemory(&m_present_parameters, sizeof(m_present_parameters));
}
Direct3D::~Direct3D()
{
}

// Accessors and Mutators
PD3DObject Direct3D::GetD3DObject()
{
	return m_d3dobject;
}
PD3DDevice Direct3D::GetD3DDevice()
{
	return m_d3ddevice;
}
PD3DXSprite Direct3D::GetD3DXSprite()
{
	return m_sprite;
}

void Direct3D::InitializeD3DInWindow(HWND& window_handle)
{
	CreateD3DObject();
	VerifyHALAndFormatSupport();
	VerifyHardwareVertexProcessing();
	SetPresentParams(window_handle);
	CreateDeviceInterface(window_handle);

	CreateSprite();
}

bool Direct3D::IsDeviceLost()
{
	HRESULT device_status = m_d3ddevice->TestCooperativeLevel();

	switch(device_status)
	{
	case D3DERR_DEVICELOST:
		// The device is lost. Wait until we have the opportunity
		// to reset it.
		DbgMsgToChannel(DC_RENDERER, L"Device Lost\n");
		Sleep(20);
		return true;
	case D3DERR_DRIVERINTERNALERROR:
		// Internal driver error. Not much we can do about this. Quit.
		DbgMsgToChannel(DC_RENDERER, L"Internal Driver Error");
		PostQuitMessage(0);
		return true;
	case D3DERR_DEVICENOTRESET:
		// The device is lost, but we can reset and restore it.
		DbgMsgToChannel(DC_RENDERER, L"Device Not Reset\n");
		OnLostDevice();
		HR(m_d3ddevice->Reset(&m_present_parameters));
		OnResetDevice();
		// We've got the device back.
		return false;
	}

	return false;
}
void Direct3D::OnLostDevice()
{
}
void Direct3D::OnResetDevice()
{
}
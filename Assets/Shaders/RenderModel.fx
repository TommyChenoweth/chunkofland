uniform extern float4x4 gWP;

struct OutputVS
{
	float4 position : POSITION0;
	float4 color	: COLOR0;
};

OutputVS SpriteVS(float3 position : POSITION0,
				  float4 color    : COLOR0)
{
	OutputVS outVS = (OutputVS)0;
	outVS.position = mul(float4(position, 1.0f), gWP);
	outVS.color = color;
	return outVS;
}

float4 SpritePS(float4 color : COLOR0) : color
{
	return color;
	//return float4(1.0f, 1.0f, 1.0f, 1.0f);
}

technique RenderModel
{
	pass P0
	{
		vertexShader = compile vs_2_0 SpriteVS();
		pixelShader  = compile ps_2_0 SpritePS();
	}
}
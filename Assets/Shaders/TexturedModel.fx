uniform extern float4x4 g_wvp;
uniform extern texture g_texture;
uniform extern texture g_normal_map;
uniform extern float3 g_light_direction;

static const float AMBIENT_INTENSITY = 0.0f;

sampler TextureSampler = sampler_state
{
	Texture = <g_texture>;
	MinFilter = ANISOTROPIC;
	MaxAnisotropy = 8;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};
sampler NormalMapSampler = sampler_state
{
	Texture = <g_normal_map>;
	MinFilter = ANISOTROPIC;
	MaxAnisotropy = 8;
	MagFilter = LINEAR;
	MipFilter = LINEAR;
	AddressU = WRAP;
	AddressV = WRAP;
};

struct OutputVS
{
	float4 position 	: POSITION0;
	float3 light_dir	: TEXCOORD1;
	float2 uv			: TEXCOORD0;
};

OutputVS SpriteVS(float3 position	: POSITION0,
				  float3 tangent	: TANGENT0,
				  float3 bitangent	: BINORMAL0,
				  float3 normal		: NORMAL0,
				  float2 uv			: TEXCOORD0)
{
	float3x3 TBN;
	TBN[0] = tangent;
	TBN[1] = bitangent;
	TBN[2] = normal;
	
	float3x3 to_tangent_space = transpose(TBN);

	OutputVS outVS = (OutputVS)0;
	outVS.position = mul(float4(position, 1.0f), g_wvp);
	outVS.light_dir = mul(g_light_direction, to_tangent_space);
	outVS.uv = uv;
	return outVS;
}

float4 SpritePS(float3 light_dir	: TEXCOORD1,
				float2 uv			: TEXCOORD0) : color
{
	float4 texture_normal = tex2D(NormalMapSampler, uv);
	texture_normal = 2*texture_normal - 1.0f;
	texture_normal = normalize(texture_normal);
	float4 texture_color = tex2D(TextureSampler, uv);
	float4 final_color = texture_color * AMBIENT_INTENSITY + texture_color * max(dot(texture_normal, light_dir), 0);
	return final_color;
}

technique TexturedModel
{
	pass P0
	{
		vertexShader = compile vs_3_0 SpriteVS();
		pixelShader  = compile ps_3_0 SpritePS();
	}
}
#pragma once

#include <boost/shared_array.hpp>
#include <boost/shared_ptr.hpp>
#include <string>

class MeshMaterial;
typedef boost::shared_ptr<MeshMaterial> PMeshMaterial;
typedef boost::shared_array<MeshMaterial> MeshMaterialArray;

class MeshMaterial
{
private:
	std::wstring m_texture_path;
	std::wstring m_normal_map_path;

public:
	// Constructors and Destructor
	MeshMaterial();
	~MeshMaterial();

	// Accessors and Mutators
	std::wstring	GetTexturePath() const;
	void			SetTexturePath(const std::wstring& texture_path);
	std::wstring	GetNormalMapPath() const;
	void			SetNormalMapPath(const std::wstring& normal_map_path);
};
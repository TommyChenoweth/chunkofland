#pragma once

#include <boost/shared_ptr.hpp>
#include <d3dx9math.h>

class Camera;
typedef boost::shared_ptr<Camera> PCamera;

class Camera
{
private:
	// View Matrix
	D3DXMATRIX	mViewMatrix;
	D3DXVECTOR3 mRight;
	D3DXVECTOR3 mUp;
	D3DXVECTOR3 mLook;
	D3DXVECTOR3 mPosition;

	// Transformation Matrices
	D3DXMATRIX mYawMatrix;
	D3DXMATRIX mPitchMatrix;
	D3DXMATRIX mRollMatrix;

	void BuildViewMatrix();
	void BuildYawMatrix(const float fRadians);
	void BuildPitchMatrix(const float fRadians);
	void BuildRollMatrix(const float fRadians);

public:
	// Constructors and Destructor
	Camera(const D3DXVECTOR3& v3Position);
	~Camera();

	// Accessors and Mutators
	D3DXVECTOR3 GetPosition() const;
	void SetPosition(const D3DXVECTOR3& v3Position);
	void Yaw(const float fRadians);
	void Pitch(const float fRadians);
	void Roll(const float fRadians);
	D3DXMATRIX GetView() const;
	D3DXVECTOR3 GetRightAxis() const;
	D3DXVECTOR3 GetUpAxis() const;
	D3DXVECTOR3 GetLookAxis() const;
};

void MoveCameraAlongAxis(PCamera camera, const D3DXVECTOR3& axis, const float units);
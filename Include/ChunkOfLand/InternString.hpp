#pragma once

#include "MurmurHash/MurmurHash3.h"
#include <string>

typedef unsigned HashedID;

const HashedID InternString(const std::wstring& str);
const HashedID InternString(const wchar_t* const c_string, const unsigned length);
#pragma once

#include <boost/shared_ptr.hpp>
#include <ChunkOfLand/D3DTypes.hpp>
#include <ChunkOfLand/MeshMaterial.hpp>
#include <string>

class IModelFileParser;
typedef boost::shared_ptr<IModelFileParser> PModelFileParser;

class IModelFileParser
{
private:
public:
	// Constructors and Destructor
	IModelFileParser();
	virtual ~IModelFileParser();

	virtual HRESULT CreateMeshFromFile(
		const std::wstring& path_to_model,
		PD3DXBuffer* out_adjacency,
		MeshMaterialArray* out_materials,
		PD3DXBuffer* out_effects,
		unsigned* out_number_of_materials,
		PD3DXMesh* out_mesh) const = 0;
};
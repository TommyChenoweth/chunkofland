#pragma once

#include <boost/shared_ptr.hpp>
#include <ChunkOfLand/D3DTypes.hpp>
#include <string>

class Timer;
typedef boost::shared_ptr<Timer> PTimer;

//! A class for measuring and rendering delta time, FPS, and MSPF.
/**
	<a href="http://msdn.microsoft.com/en-us/library/windows/desktop/ms644904%28v=vs.85%29.aspx">QueryPerformanceCounter()</a> 
	is used for high precision time measurement. Rendering is done with an 
	<a href="http://msdn.microsoft.com/en-us/library/windows/desktop/bb174249%28v=vs.85%29.aspx">ID3DXSprite</a> 
	interface. Textual data is updated once per second.
*/
class Timer
{
private:
	// Text Display Variables
	PD3DXFont		m_font;
	PD3DXSprite		m_sprite;
	D3DCOLOR		m_text_color;
	std::wstring	m_text_buffer;
	float			m_fps;
	float			m_ms_per_frame;

	// Timer Control Variables
	int				m_pause_count;
	bool			m_paused;
	bool			m_resumed;

	// Time Tracking Variables
	__int64			m_counts_this_frame;
	__int64			m_counts_last_frame;
	float			m_seconds_per_count;
	float			m_dt;

	void CreateFont(PD3DDevice d3ddevice);
	__int64 GetCounts();

public:
	// Constructors and Destructor
	Timer(PD3DDevice d3ddevice, PD3DXSprite sprite);
	~Timer();

	// Accessors and Mutators
	float GetDT() const;
	void SetTextColor(const D3DCOLOR& text_color);
	void Pause();
	void Resume();
	bool IsPaused() const;

	void UpdateTimer();
	void DrawTimerText() const;
};
#pragma once

#include <boost/shared_ptr.hpp>
#include <ChunkOfLand/Camera.hpp>
#include <ChunkOfLand/D3DTypes.hpp>
#include <ChunkOfLand/Model.hpp>
#include <ChunkOfLand/VertexDeclarations.hpp>

class Renderer;
typedef boost::shared_ptr<Renderer> PRenderer;

class Renderer
{
private:
	PD3DDevice	m_d3ddevice;

	D3DXMATRIX	m_projection;
	PCamera		m_camera;

	// Effect Variables
	PD3DXEffect	m_effect;
	D3DXHANDLE	m_technique_handle;
	D3DXHANDLE	m_wvp_matrix;
	D3DXHANDLE	m_texture_handle;
	D3DXHANDLE	m_normal_map_handle;
	D3DXHANDLE	m_light_direction_handle;
	
	D3DXVECTOR3 m_light_direction;

	PD3DXMesh	m_mesh;
	PModel		m_model;

	void CompileEffect();

public:
	// Constructors and Destructor
	Renderer(PD3DDevice d3ddevice, const unsigned window_width, const unsigned window_height);
	~Renderer();

	// Accessors and Mutators
	void AddCamera(PCamera& camera);
	void RemoveCamera(PCamera& camera);
	void SetDirectionalLight(const D3DXVECTOR3& light_direction);
	D3DXVECTOR3 GetDirectionalLight() const;

	void RenderGeometry();
};

#pragma once

#include <ChunkOfLand/Camera.hpp>
#include <ChunkOfLand/D3DTypes.hpp>
#include <ChunkOfLand/Direct3D.hpp>
#include <ChunkOfLand/PlayerInput.hpp>
#include <ChunkOfLand/Renderer.hpp>
#include <ChunkOfLand/Timer.hpp>
#include <ChunkOfLand/Window.hpp>

class ChunkOfLand
{
private:
	PWindow			m_window;
	PDirect3D		m_direct3d;
	PTimer			m_timer;
	PRenderer		m_renderer;
	PCamera			m_camera;
	PPlayerInput	m_player_input;

	PD3DDevice	m_d3ddevice;
	PD3DObject	m_d3dobject;
	PD3DXSprite	m_sprite;

public:
	// Constructors and Destructor
	ChunkOfLand();
	~ChunkOfLand();

	int Run();
	void Update(const float dt);
	void Render();
};
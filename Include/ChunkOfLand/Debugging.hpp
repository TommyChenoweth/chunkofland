#pragma once

#include <d3dx9.h>
#include <D3DX9Mesh.h>
#include <dxerr.h>
#include <string>

//#if defined(DEBUG) | defined(_DEBUG)
	#ifndef HR
	#define HR(x)												\
	{															\
		HRESULT hr = x;											\
		if(FAILED(hr))											\
		{														\
			DXTrace(__FILE__, (DWORD)__LINE__, hr, L#x, TRUE);	\
		}														\
	}
	#endif
//
//#else
//	#ifndef HR
//	#define HR(x) x;
//	#endif
//#endif 

// Active Debug Channels
extern int g_DbgChannels;

// Debug Channels
static const int DC_ASSETMANAGEMENT = 0x1;
static const int DC_INPUT = 0x2;
static const int DC_EVENTS = 0x4;
static const int DC_RENDERER = 0x8;
static const int DC_TIMING = 0x10;

// Output a formatted debug message to a channel.
void DbgMsgToChannel(const int nChannel, const wchar_t* chpMessage, ...);

void DbgMsgToChannel(const int channel, ID3DXBuffer* const error_message);
#pragma once

#include <boost/shared_ptr.hpp>
#include <Windows.h>

class PlayerInput;
typedef boost::shared_ptr<PlayerInput> PPlayerInput;

//! A class for detecting player input.
/**
	This class is the purveyor of all player input. Input is detected via
	polling. Call PollKeyboard to get the state of the keyboard, and then
	determine the state of individual keys with the accessor functions.
*/
class PlayerInput
{
private:
	// Key Data
	BYTE m_key_state_prev[256];
	BYTE m_key_state_curr[256];

	// Mouse Position Data
	POINT	m_mouse_position_prev;
	POINT	m_mouse_position_curr;
	POINT	m_mouse_position_delta;
	bool	m_mouse_position_locked;

	HWND m_window_handle;

	void CurrentToPrevious();
	void PollKeyboard();
	void PollMouse();

public:
	// Constructors and Destructor
	PlayerInput(HWND handle);
	~PlayerInput();
	
	// Accessors and Mutators
	bool IsKeyUp(const int key);
	bool IsKeyDown(const int key);
	bool IsKeyPressed(const int key);
	bool IsKeyReleased(const int key);
	POINT GetMousePos();
	POINT GetMousePosDelta();
	void LockMousePos(const bool lock);
	bool IsMousePosLocked();

	void PollInputDevices();
};
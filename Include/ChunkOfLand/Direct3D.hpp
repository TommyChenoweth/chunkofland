#pragma once

#include <boost/shared_ptr.hpp>
#include <ChunkOfLand/D3DTypes.hpp>
#include <d3d9.h>
#include <windows.h>

class Direct3D;
typedef boost::shared_ptr<Direct3D> PDirect3D;

class Direct3D
{
private:
	// Direct3D Objects
	PD3DDevice	m_d3ddevice;
	PD3DObject	m_d3dobject;
	PD3DXSprite	m_sprite;

	// Direct3D Options
	UINT					m_adapter;
	D3DFORMAT				m_adapter_format;
	DWORD					m_vertex_processing;
	DWORD					m_device_behavior;
	D3DDEVTYPE				m_device_type;
	D3DFORMAT				m_backbuffer_format;
	D3DPRESENT_PARAMETERS	m_present_parameters;

	// Initializing Direct3D
	HRESULT CreateD3DObject();
	HRESULT VerifyHALAndFormatSupport();
	HRESULT VerifyHardwareVertexProcessing();
	void	SetPresentParams(HWND& window_handle);
	HRESULT CreateDeviceInterface(HWND& window_handle);

	void CreateSprite();

public:
	// Constructors and Destructor
	Direct3D();
	~Direct3D();

	// Accessors and Mutators
	PD3DObject GetD3DObject();
	PD3DDevice GetD3DDevice();
	PD3DXSprite GetD3DXSprite();

	void InitializeD3DInWindow(HWND& hWindow);

	bool IsDeviceLost();
	void OnLostDevice();
	void OnResetDevice();
};
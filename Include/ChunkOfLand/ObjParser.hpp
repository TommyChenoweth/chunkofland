#pragma once

#include <boost/unordered_map.hpp>
#include <ChunkOfLand/IModelFileParser.hpp>
#include <ChunkOfLand/InternString.hpp>
#include <ChunkOfLand/D3DTypes.hpp>
#include <fstream>

class ObjParser;
typedef boost::shared_ptr<ObjParser> PObjParser;

class ObjParser : public IModelFileParser
{
public:
	class VertexIndexData
	{
	public:
		short absolute_index;
		short position_vertex_index;
		short uv_coordinate_index;
		short normal_vertex_index;
	};
private:
	typedef boost::unordered_map<HashedID, unsigned>	TokenToCountMap;
	typedef boost::unordered_map<HashedID, VertexIndexData> VertexIndices;

	PD3DDevice	m_d3ddevice;

	void CreateMaterialsFromFile(MeshMaterialArray* out_materials, const std::wstring& path_to_file) const;
	TokenToCountMap CountAllTokensInFile(std::wifstream* file) const;

public:
	// Constructors and Destructor
	ObjParser(PD3DDevice d3ddevice);
	~ObjParser();

	// IModelFileParser Implementation
	HRESULT CreateMeshFromFile(
		const std::wstring& path_to_model,
		PD3DXBuffer* out_adjacency,
		MeshMaterialArray* out_materials,
		PD3DXBuffer* out_effects,
		unsigned* out_number_of_materials,
		PD3DXMesh* out_mesh) const;
};
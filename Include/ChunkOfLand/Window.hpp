#pragma once

#include <boost/shared_ptr.hpp>
#include <string>
#include <windows.h>


class Window;
typedef boost::shared_ptr<Window> PWindow;

class Window
{
private:
	// Window Variables
	HINSTANCE		m_instance;
	std::wstring	m_title;
	unsigned		m_window_width;
	unsigned		m_window_height;
	bool			m_active;
	HWND			m_handle;

	void InitWindow();

public:
	// Constructors and Destructor
	Window(const std::wstring& title, const unsigned window_width, const unsigned window_height);
	~Window();

	// Accessors and Mutators
	HWND& GetHWND(){return m_handle;}
	bool IsActive(){return m_active;}
	unsigned GetWidth(){return m_window_width;}
	void SetWidth(const unsigned window_width);
	unsigned GetHeight(){return m_window_height;}
	void SetHeight(const unsigned window_height);

	LRESULT msgProc(UINT msg, WPARAM wParam, LPARAM lParam);
};
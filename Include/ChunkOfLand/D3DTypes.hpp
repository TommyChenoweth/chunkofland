#pragma once

#include <boost/mem_fn.hpp>
#include <boost/shared_array.hpp>
#include <boost/shared_ptr.hpp>
#include <d3d9.h>
#include <d3dx9core.h>
#include <d3dx9mesh.h>

typedef boost::shared_ptr<IDirect3D9>					PD3DObject;
typedef boost::shared_ptr<IDirect3DDevice9>				PD3DDevice;
typedef boost::shared_ptr<ID3DXSprite>					PD3DXSprite;
typedef boost::shared_ptr<ID3DXFont>					PD3DXFont;
typedef boost::shared_ptr<IDirect3DVertexDeclaration9>	PD3DVertexDeclaration;
typedef boost::shared_ptr<IDirect3DStateBlock9>			PD3DStateBlock;
typedef boost::shared_ptr<ID3DXEffect>					PD3DXEffect;
typedef boost::shared_ptr<ID3DXMesh>					PD3DXMesh;
typedef boost::shared_ptr<IDirect3DTexture9>			PD3DTexture;
typedef boost::shared_ptr<ID3DXBuffer>					PD3DXBuffer;

template <typename T>
void BuildD3DPtr(boost::shared_ptr<T>* p, T** c)
{
	p->reset(*c, boost::mem_fn(&T::Release));
	*c = 0;
}
#pragma once

#include <boost/shared_ptr.hpp>
#include <ChunkOfLand/D3DTypes.hpp>
#include <ChunkOfLand/IModelFileParser.hpp>
#include <string>
#include <vector>

class Model;
typedef boost::shared_ptr<Model> PModel;

class Model
{
private:
	PD3DDevice	m_d3ddevice;

	PD3DXMesh			m_mesh;
	PD3DXBuffer			m_adjacency;
	MeshMaterialArray	m_materials;
	PD3DXBuffer			m_effects;
	unsigned			m_number_of_materials;

	PD3DTexture	m_color_map;
	PD3DTexture	m_normal_map;

	std::vector<PD3DTexture>	m_textures;

	PModelFileParser	m_parser;
	std::wstring		m_path_to_model;

	void LoadTextures();

public:
	// Constructors and Destructor
	Model(PD3DDevice d3ddevice, PModelFileParser parser, const std::wstring& path_to_model);
	~Model();

	// Accessors and Mutators
	PD3DXMesh	GetMesh();
	unsigned	GetNumberOfSubsets() const;
	PD3DTexture	GetTextureForSubset(const unsigned subset_index);
	PD3DTexture	GetNormalMapForSubset(const unsigned subset_index);

	void Load();
};